Name:           libcroco
Version:        0.6.13
Release:        3
Summary:        A CSS parsing library
License:        LGPLv2
URL:            https://formulae.brew.sh/formula/libcroco
Source0:        https://gitlab.gnome.org/Archive/libcroco/-/archive/%{version}/%{name}-%{version}.tar.gz

Patch0:      CVE-2017-8834_CVE-2017-8871.patch
Patch1:      backport-CVE-2020-12825-parser-limit-recursion-in-block-and-any-productions.patch

BuildRequires:  git gcc glib2-devel libxml2-devel pkgconfig gtk-doc libtool

%description
Libcroco is a standalone css2 parsing and manipulation library.
The parser provides a low level event driven SAC like api
and a css object model like api.
Libcroco provides a CSS2 selection engine and an experimental
xml/css rendering engine.

%package        devel
Summary:        Files for %{name} development
Requires:       %{name} = %{version}-%{release}
%description    devel
Files for %{name} development

%package_help

%prep
%autosetup -n %{name}-%{version} -p1 -Sgit
sh autogen.sh

%build
%configure
%make_build CFLAGS="$CFLAGS -fno-strict-aliasing"

%install
%make_install
%delete_la_and_a

%check
make check
make test

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%license COPYING COPYING.LIB
%{_bindir}/csslint*
%{_libdir}/%{name}*.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/croco*
%{_libdir}/%{name}*.so
%{_libdir}/pkgconfig/%{name}*.pc
%{_includedir}/%{name}*

%files help
%defattr(-,root,root)
%doc README NEWS AUTHORS


%changelog
* Tue Jan 12 2021 zoulin<zoulin13@huawei.com> - 0.6.13-3
- Type:CVE
- CVE:CVE-2020-12825
- SUG:NA
- DESC:Replace the patch of CVE-2020-12825

* Thu Sep 10 2020 wangchen<wangchen137@huawei.com> - 0.6.13-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:modify the URL of Source0

* Thu Jul 16 2020 yang_zhuang_zhuang<yangzhuangzhuang1@huawei.com> - 0.6.13-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 0.6.13

* Sun Jun 28 2020 wangchen<wangchen137@huawei.com> - 0.6.12-14
- Type:cves
- ID:CVE-2020-12825
- SUG:NA
- DESC:fix CVE-2020-12825

* Mon Jan 20 2020 hanxinke<hanxinke@huawei.com> - 0.6.12-13
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add build requires for libtool

* Wed Jan 8 2020 hanxinke<hanxinke@huawei.com> - 0.6.12-12
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove useless patch

* Tue Dec 31 2019 hanxinke<hanxinke@huawei.com> - 0.6.12-11
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update source package

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.6.12-10
- Fix CVE-2017-8834 CVE-2017-8871

* Sat Sep 28 2019 shenyangyang <shenyangyang4@huawei.com> - 0.6.12-9
- Type:NA
- ID:NA
- SUG:NA
- DESC:enable test

* Thu Aug 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.6.12-8
- Type:NA
- ID:NA
- SUG:NA
- DESC:modify spec file

* Wed Aug 21 2019 fangyufa <fangyufa1@huawei.com> - 0.6.12-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify name of patch

* Thu Apr 25 2019 wangchan <wangchan9@huawei.com> - 0.6.12-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:input: check end of input before reading a byte
       tknzr: support only max long rgb values

* Fri Jul 13 2018 openEuler Buildteam <buildteam@openeuler.org> - 0.6.12-5
- Package init
